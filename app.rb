require "json/ext"
require "mongoid"
require "mongoid-pagination"
require "sinatra"
#require "sinatra/accept_params"
require 'sinatra/cross_origin'
#require "slim"
require "bcrypt"
require "securerandom"

class FAJ < Sinatra::Application

  if defined? Encoding
    Encoding.default_external = Encoding::UTF_8
    Encoding.default_internal = Encoding::UTF_8
  end

  #configure :production do
  #  Mongoid.load!('./config/mongoid.yml', :production)
  #end

  configure :development do
    Mongoid.load!('./config/mongoid.yml', :development)
  end

  configure do
    set :root, File.dirname(__FILE__)
    set :app_file, __FILE__
    set :default_encoding, 'utf-8'
    set :static, true
    enable :cross_origin
    set :allow_origin, :any
    set :allow_methods, [:post, :get, :options, :delete]
    set :allow_credentials, true
    set :max_age, "1728000"
  end

end

Dir[File.join(File.dirname(__FILE__), "app", "**", "init.rb")].each do |file|
  require file
end
