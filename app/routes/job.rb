class FAJ < Sinatra::Application

  # ------------------
  # REST API for recipes
  # ------------------

  get '/jobs', :provides => :json do
    content_type :json
    page = params['page_number'] || 1
    author = params['author'].nil? || params['author']['username'].nil? ? nil : User.where(username: params['username'])
    jobs = author.nil? || author == [] ? Job.paginate(:page => page, :limit => 10).desc(:_id) : author.first.jobs.where(closed: false)

    unless jobs == []
      status 200
      body(
        JSON.generate( 
          {:jobs => jobs.map { |j| 
            {
              :id => j._id.to_s,
              :title => j.title,
              :description => j.description,
              :created_at => j.created_at,
              :applicants => j.applicants,
              :author => j.user
            }
          }
        })
      )
    else
      status 404
    end
  end  

  get "/jobs/:id", :provides => :json do
    content_type :json

    jobs = Jon.where(_id: params[:id])
    unless users == []
      job = jobs.first
      status 200
      body(
        JSON.generate({
          :id => job._id.to_s,
          :title => job.title,
          :description => job.description,
          :created_at => job.created_at,
          :applicants => job.applicants,
          :author => job.user
        })
      )
    else
      status 404
    end
  end

  post "/jobs/?", :provides => :json do
    content_type :json

    data = request.body.read
    jd = JSON.parse(data)
    users = User.where(username: jd['username']) if jd['username']
    if ['title', 'description', 'username'].all? {|o| jd.include? o} && users != []
      user = users.first
      job = Job.new({:title => jd['title'], :description => jd['description']})
      job.closed = false
      job.created_at = Date.new
      if job.save
        job.author = user
        status 201
      else
        status 400
      end
    else
      status 400
    end
  end

  put "/jobs/:id", :provides => :json do
    content_type :json

    data = request.body.read
    jd = JSON.parse(data)
    new_params = Hash[*jd.select {|k,v| ['title', 'description', 'closed'].include?(k)}.flatten]
    jobs = Job.where(_id: params[:id])
    unless (jobs == [])
      job = job.first
      user = job.user
      unless (new_params == {} || jd['auth_token'].nil? || user.token != jd['auth_token'])
        job.update_attributes(new_params)
        if job.save
          status 200
          job.to_json
        else
          status 400
        end
      else
        status 400
      end
    else
      status 404
    end
  end

  # delete "/users/:id/?", :provides => :json do
  #   content_type :json

  #   users = User.where(_id: params[:id])
  #   unless users == []
  #     users.first.delete
  #     status 204 # No content
  #   else
  #     status 404
  #   end
  # end

end