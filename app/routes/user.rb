class FAJ < Sinatra::Application

  # ------------------
  # REST API for users
  # ------------------

  get '/users', :provides => :json do
    content_type :json
    limit = 20
    page = params['page_number'] || 1
    #users = User.only(:username, :realname, :avatar, :created_at).paginate(:page => page, :limit => limit).desc(:_id)
    users = User.only(:username, :realname, :avatar, :created_at).desc(:_id)
    unless users == []
      status 200
      body(
        JSON.generate( 
          {:users => users.map { |u| 
            {
              :id => u._id.to_s,
              :username => u.username,
              :realname => u.realname,
              :avatar => u.avatar,
              :email => u.email,
              :created_at => u.created_at
            }
          }#,
          #:pages => users.count/limit+1
        })
      )
    else
      status 404
    end
  end  

  get "/users/:id", :provides => :json do
    content_type :json

    users = User.only(:username, :realname, :avatar, :created_at).where(_id: params[:id])
    unless users == []
      user = users.first
      status 200
      body(
        JSON.generate({
          :id => user._id.to_s,
          :username => user.username,
          :realname => user.realname,
          :avatar => user.avatar,
          :created_at => user.created_at
        })
      )
    else
      status 404
    end
  end

  post "/users/?", :provides => :json do
    content_type :json

    data = request.body.read
    jd = JSON.parse(data)
    if jd['user'] && ['username', 'realname', 'password', 'email', 'avatar'].all? {|o| jd['user'].include? o}
      users = User.where("$or" => [{'email' => jd['user']['email']}, {'username' => jd['user']['username']}])
      if users.count == 0
        user = User.new(Hash[*jd['user'].select {|k,v| ['username', 'realname', 'email', 'avatar'].include?(k)}.flatten])
        user.salt = BCrypt::Engine.generate_salt
        user.password = BCrypt::Engine.hash_secret(jd['user']['password'], user.salt)
        user.token = SecureRandom.hex
        user.remember_token = SecureRandom.hex
        user.created_at = Date.new
        if user.save
          status 201
        else
          status 400
        end
      else
        status 400
      end
    else
      status 400
    end
  end

  put "/users/:id", :provides => :json do
    content_type :json

    data = request.body.read
    jd = JSON.parse(data)
    new_params = jd['user'] ? Hash[*jd['user'].select {|k,v| ['realname', 'avatar', 'email'].include?(k) && v }.flatten] : {}
    users = User.where(_id: params[:id])
    unless users == []
      user = users.first
      unless new_params == {}
        unless new_params['email'] && User.where(email: new_params['email']) != []
          user.update_attributes!(new_params)
          status 200
          user.to_json
        else
          status 404
        end
      else
        status 400
      end
    else
      status 404
    end
  end

  # delete "/users/:id/?", :provides => :json do
  #   content_type :json

  #   users = User.where(_id: params[:id])
  #   unless users == []
  #     users.first.delete
  #     status 204 # No content
  #   else
  #     status 404
  #   end
  # end

end