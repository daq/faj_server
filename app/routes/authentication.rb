class FAJ < Sinatra::Application

  # --------------
  # Authentication
  # --------------

  post '/signin' do
    data = request.body.read
    jd = JSON.parse(data)
    if jd['email'] 
      if user = User.where(email: jd['email']).first 
        if user.password == BCrypt::Engine.hash_secret(jd['password'], user.salt)
          status 200
          reply = {"auth_token" => user.token, "user_id" => user._id.to_s}
          reply['remember_token'] = user.remember_token if jd['remember']
          body JSON.generate(reply)
        else
          status 401
        end
      end
    elsif jd['remember_token']
      if user = User.where(remember_token: jd['remember_token']).first 
        status 200
        reply = {"auth_token" => user.token, "user_id" => user._id.to_s}
        reply['remember_token'] = user.remember_token if jd['remember']
        body JSON.generate(reply)
      else
        status 401
      end
    else
      status 401
    end
  end

  delete "/signout" do
    data = request.body.read
    jd = JSON.parse(data)
    if ['auth_token'].all? {|o| jd.include? o}
      if user = User.where(:token => jd['auth_token']).first
        status 200
        reply = {"auth_token" => user.token}#, "user_id" => user._id.to_s}
        body JSON.generate(reply)
      else
        status 401
      end
    else
      status 400
    end
  end

end