class FAJ < Sinatra::Application

  get '/' do
    File.read('public/index.html')
  end

  options '/*' do
    status 200
    response.headers['Access-Control-Allow-Headers'] = 'origin, content-type, accept'
  end

end