class Job
  include Mongoid::Document
  include Mongoid::Pagination
    
  field :title, type: String
  field :description, type: String
  field :created_at, type: DateTime
  filed :closed, type: Boolean

  embeds_one :user, store_as: 'selected'

  belongs_to :user
  has_many :applicants, class_name => 'User', :inverse_of => :applied_jobs, :autosave => true

  validates :title, :presence => true, :allow_blank => false
  validates :description, :presence => true, :allow_blank => false

end