class User
  include Mongoid::Document
  include Mongoid::Pagination
    
  field :username, type: String
  field :realname, type: String
  field :password, type: String
  field :salt, type: String
  field :token, type: String
  field :remember_token, type: String
  field :email, type: String
  field :contacts, type: String
  field :avatar, type: String
  field :created_at, type: DateTime

  has_many :jobs
  has_many :applied_jobs, class_name => 'Job', :inverse_of => :applicants, :autosave => true

  embedded_in :job

  validates :username, :presence => true, :length => { :minimum => 3, :maximum => 20, :allow_blank => false }, uniqueness: true
  validates :realname, :presence => true, :length => { :minimum => 3, :maximum => 64, :allow_blank => false }
  validates :password, :presence => true, :allow_blank => false
  validates :salt, :presence => true, :allow_blank => false
  validates :token, :presence => true, :allow_blank => false, uniqueness: true
  validates :remember_token, :presence => true, :allow_blank => false, uniqueness: true
  validates :email, :presence => true, :length => { :minimum => 6, :maximum => 96, :allow_blank => false }, uniqueness: true

  def apply!(job)
    if !self.applied_jobs.include?(job)
      self.applied_jobs << job
    end
  end

  def disapply!(job)
    if self.applied_jobs.include?(job)
      self.applied_jobs.delete(job)
    end
  end

  def select!(job, user)
    if self.jobs.include?(job) && job.applicants.include?(user)
      job.selected = user
    end
  end

end