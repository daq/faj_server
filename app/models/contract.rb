class Contract
  include Mongoid::Document
    

  belongs_to :user
  has_many :applicants, class_name => 'User', :inverse_of => :applied_jobs

  validates :title, :presence => true, :allow_blank => false
  validates :description, :presence => true, :allow_blank => false

end