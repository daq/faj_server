conn = new Mongo();
db = conn.getDB('faj');
db.dropDatabase();
db = conn.getDB('faj');
coll = db.users;
coll.save({username: 'test', realname: 'Test Testov', password: '$2a$10$.8uo5TD3lq5Hw2u6GjBHg./eI.SNWwruzfOy13GnDZWRUuq86D3m2', salt: '$2a$10$.8uo5TD3lq5Hw2u6GjBHg.', token: '33651e45ee8e2740d914a405e6cf5588', remember_token: '906e20d8e3801f972baa25af5f1cdee1', email: 'test@test.ru', avatar: 'avatar.jpg', created_at: new Date()});
coll.save({username: 'test0', realname: 'Test0 Testov', password: '$2a$10$Q3dKYt44agZj1JCeKTbiZONKXPoWoc9yBNpy.OznEwpYSi3/GGfge', salt: '$2a$10$Q3dKYt44agZj1JCeKTbiZO', token: '196314752fc2ef4e87f9fa87ab62f05a', remember_token: '154ef4d201176d0716ebe668bbd74911', email: 'test0@test.ru', avatar: 'avatar0.jpg', created_at: new Date()});

coll.save({username: 'test1', realname: 'Test1 Testov', password: '$2a$10$DuYGWr4r0MWbcnBG3cnmHuM2Uc.7WrfetnsS.HHhjKk8BUdHlAcIW', salt: '$2a$10$DuYGWr4r0MWbcnBG3cnmHu', token: '509c2e3e3508c09721819b0d93c60e75', remember_token: '723ec64da5958fe9c7b253431389fc5e', email: 'test1@test.ru', avatar: 'avatar1.jpg', created_at: new Date()});

coll.save({username: 'test2', realname: 'Test2 Testov', password: '$2a$10$GPWFCDBjLawGhz47MpjRTehx/QChQgivAV59ZXMDafklWxb/0DgE6', salt: '$2a$10$GPWFCDBjLawGhz47MpjRTe', token: '526fac28ab7d1b730d0c15ef28a615e5', remember_token: 'd4683a3ccfcd572c8be2d9b9377636d7', email: 'test2@test.ru', avatar: 'avatar2.jpg', created_at: new Date()});

coll.save({username: 'test3', realname: 'Test3 Testov', password: '$2a$10$jKCxXfXji63g.RHdc2idnengyaMqfgMhXYennZrvATraFLUENH.c.', salt: '$2a$10$jKCxXfXji63g.RHdc2idne', token: '7aae4db5208e600fc86b7a655bd604b9', remember_token: '8d38fb42ba454bf8c7c7a289df4d58d8', email: 'test3@test.ru', avatar: 'avatar3.jpg', created_at: new Date()});

coll.save({username: 'test4', realname: 'Test4 Testov', password: '$2a$10$Zv1fDxr2bzWJ3sY/vwe61eU2BjuuUpDuu9oQoVoBtQsqrvGR7qSmu', salt: '$2a$10$Zv1fDxr2bzWJ3sY/vwe61e', token: '5bfaa0c4e10bd3a3ab4225cdcb550a9b', remember_token: '6571f5e14fd63aeb2a8fca30c8844a2f', email: 'test4@test.ru', avatar: 'avatar4.jpg', created_at: new Date()});

coll.save({username: 'test5', realname: 'Test5 Testov', password: '$2a$10$vNcWiJguxVNsxyZ3OEfhjOVxZ676nE69vEtmv4a2/vfKUC5EYUkOK', salt: '$2a$10$vNcWiJguxVNsxyZ3OEfhjO', token: 'e76000a62b24d0881870a27b286de47e', remember_token: 'd7fb17c41cee41a5bfa5ed3053772789', email: 'test5@test.ru', avatar: 'avatar5.jpg', created_at: new Date()});

coll.save({username: 'test6', realname: 'Test6 Testov', password: '$2a$10$CykwlnGbLCHi7qRVF.KbQeLcvI/v2ZNCNxf9Z4UzOBoCWte4ALdfO', salt: '$2a$10$CykwlnGbLCHi7qRVF.KbQe', token: 'e4bedcc6a57783f5f2263db976292f17', remember_token: '60902e4e7a252de11016d80e7bcf5654', email: 'test6@test.ru', avatar: 'avatar6.jpg', created_at: new Date()});

coll.save({username: 'test7', realname: 'Test7 Testov', password: '$2a$10$jZI.9w5N3kk19GDFoAnIOOR6zeyjZt1gP88YUyNWRvAGf84Jv7R.G', salt: '$2a$10$jZI.9w5N3kk19GDFoAnIOO', token: '384eae44b08440ca7081acba88b954da', remember_token: '4c297c3312691153f4befa3e911f6dda', email: 'test7@test.ru', avatar: 'avatar7.jpg', created_at: new Date()});

coll.save({username: 'test8', realname: 'Test8 Testov', password: '$2a$10$Y0/DUCHn28/Gn7WUPlmR..pwtRF8HXF4d6G8Vvby7f8ezi7Qk6u9K', salt: '$2a$10$Y0/DUCHn28/Gn7WUPlmR..', token: 'd44fe979daa76e32c21a3ee34680b87a', remember_token: '2f12e7dcb3e04259baaad6179eeea1dd', email: 'test8@test.ru', avatar: 'avatar8.jpg', created_at: new Date()});

coll.save({username: 'test9', realname: 'Test9 Testov', password: '$2a$10$ufet3NFtH1eCy8uiAMfU6OHEDZNVol4NXR.xouPICXvnwYvmxvRdS', salt: '$2a$10$ufet3NFtH1eCy8uiAMfU6O', token: 'eebd0f12549a3c2e86a7e8087361e569', remember_token: '5f2a8c869ee05bf99856d685a87084f0', email: 'test9@test.ru', avatar: 'avatar9.jpg', created_at: new Date()});

coll.save({username: 'test10', realname: 'Test10 Testov', password: '$2a$10$Ga4THJyxjR96yj.aNHkTlOCI1fhto1te3bvU0m84BiSODGjJa4aJm', salt: '$2a$10$Ga4THJyxjR96yj.aNHkTlO', token: '59c8f132ee3d4b9a2bb0b056456d597b', remember_token: '255cedbf8b445bfd042a61b9a0b67eb2', email: 'test10@test.ru', avatar: 'avatar10.jpg', created_at: new Date()});

coll.save({username: 'test11', realname: 'Test11 Testov', password: '$2a$10$79R/yfzj20ACHcpbeBoUc.X2GhzkCGJAcjr6bPXUQYSkZJJ/2FN1K', salt: '$2a$10$79R/yfzj20ACHcpbeBoUc.', token: 'b83797d1133a190f05786ae7ccaf942c', remember_token: 'cab30f2ad7ff70aace4896f4c9b5c505', email: 'test11@test.ru', avatar: 'avatar11.jpg', created_at: new Date()});

coll.save({username: 'test12', realname: 'Test12 Testov', password: '$2a$10$i0ZK.WAQvKWlYZ8zzbYRoedtfq7W7qhweOWVJnXITB66j1nIkIcUO', salt: '$2a$10$i0ZK.WAQvKWlYZ8zzbYRoe', token: '1a96dae134e2513fd9ee97fe81a3b2cb', remember_token: '7a5e80907a08ddf83c02ddc0b6c2b176', email: 'test12@test.ru', avatar: 'avatar12.jpg', created_at: new Date()});

coll.save({username: 'test13', realname: 'Test13 Testov', password: '$2a$10$GfqJ/niPdSmulL2wBhzererFjZHKtX0nzZ1JTJ3z3fkGbYUhfbePu', salt: '$2a$10$GfqJ/niPdSmulL2wBhzere', token: 'ac27972db3b3ec11d400063aac55e11b', remember_token: '217ce15fd9d8c48dd987a9eeea9b6116', email: 'test13@test.ru', avatar: 'avatar13.jpg', created_at: new Date()});

coll.save({username: 'test14', realname: 'Test14 Testov', password: '$2a$10$JTG89jv5wy9Dr3cYnT8hz.YIDEao/z3nAit60Uo7ccBsylgLl2fxG', salt: '$2a$10$JTG89jv5wy9Dr3cYnT8hz.', token: '4de0da071bda0c6a74631a41fa6d955a', remember_token: '86a13e6ebc91cf9e99bdb9fe36d256c5', email: 'test14@test.ru', avatar: 'avatar14.jpg', created_at: new Date()});

coll.save({username: 'test15', realname: 'Test15 Testov', password: '$2a$10$6zpSNU7JQRBeGLZnL2YZLu.hNnYAv.e0Mg.X1kybar4aOPgKGSPJ6', salt: '$2a$10$6zpSNU7JQRBeGLZnL2YZLu', token: '60dad2d46b15d46d46ec6076dabd06d8', remember_token: 'caf7aad3ecf9b7155e5313d549d1db5e', email: 'test15@test.ru', avatar: 'avatar15.jpg', created_at: new Date()});

coll.save({username: 'test16', realname: 'Test16 Testov', password: '$2a$10$D.0TlSSLDXAQ81gpv.krcux.QmBi8E2V2f/Nj3mMBxfvlfDITL7Ea', salt: '$2a$10$D.0TlSSLDXAQ81gpv.krcu', token: '141492b075a1defa471c251484ba5e0e', remember_token: '5cf48922022a05eca801742471a7509c', email: 'test16@test.ru', avatar: 'avatar16.jpg', created_at: new Date()});

coll.save({username: 'test17', realname: 'Test17 Testov', password: '$2a$10$WJX3opedTLzElxIeXl1QF.5rNyVyfOVKOHBdVOrMfxzBQjZAs6eou', salt: '$2a$10$WJX3opedTLzElxIeXl1QF.', token: 'e0092f60efbe96070ffee82bbf1acb86', remember_token: 'e3332d7f970184b9954bc51d377b3cdb', email: 'test17@test.ru', avatar: 'avatar17.jpg', created_at: new Date()});

coll.save({username: 'test18', realname: 'Test18 Testov', password: '$2a$10$MJ/mX/Z0CjnncFiqWoF5p.FOsHvwHqcngPfqY7wFy7DTCMydl1ZVy', salt: '$2a$10$MJ/mX/Z0CjnncFiqWoF5p.', token: '833aed914aa01acbf43f6a3381280a99', remember_token: '45728bb7e25b7233353e73940e665f58', email: 'test18@test.ru', avatar: 'avatar18.jpg', created_at: new Date()});

coll.save({username: 'test19', realname: 'Test19 Testov', password: '$2a$10$Xa2zeMDGWTZ5SzHtZ/Qrm.3wZ.LEn.VfyWSAvCf66qOFZMfUetLra', salt: '$2a$10$Xa2zeMDGWTZ5SzHtZ/Qrm.', token: 'f536e8f0c9213deefa0af733d49993a2', remember_token: '9a5a6cf4bedeae96d09a4a5455c087ed', email: 'test19@test.ru', avatar: 'avatar19.jpg', created_at: new Date()});

coll.save({username: 'test20', realname: 'Test20 Testov', password: '$2a$10$Xft2XiQid683RFwMfDHypuLKYjXfWl8efSA.G7tANOiuvReP8lnFS', salt: '$2a$10$Xft2XiQid683RFwMfDHypu', token: '692ccd7d73a0fd3f4d8494053a022709', remember_token: 'fef546985b9a99aef1755629954e85ad', email: 'test20@test.ru', avatar: 'avatar20.jpg', created_at: new Date()});

coll.save({username: 'test21', realname: 'Test21 Testov', password: '$2a$10$c39vg/9.0vMRS35tKSx5euLc/OXlG/L9zOXBUH2Az/Gck5INhyY7e', salt: '$2a$10$c39vg/9.0vMRS35tKSx5eu', token: '6c085bd68a50b35ec85db157c55db63c', remember_token: 'd9a3d98f8db5fefecf25f093ed2da41e', email: 'test21@test.ru', avatar: 'avatar21.jpg', created_at: new Date()});

coll.save({username: 'test22', realname: 'Test22 Testov', password: '$2a$10$u/obChWsOcZ4.xJKxR8B1uwSrmq1clGTycb5yNj43lqNbo6403uKK', salt: '$2a$10$u/obChWsOcZ4.xJKxR8B1u', token: '04920ef861e6406da545459ffb3bd6f1', remember_token: '831c4f3e30cb000f0c686843d613aa92', email: 'test22@test.ru', avatar: 'avatar22.jpg', created_at: new Date()});

coll.save({username: 'test23', realname: 'Test23 Testov', password: '$2a$10$uZuKQpHyqP0ZfDBPEG/Ine2OrCEQi5/4X9SE0n5iULtfVRyFrJ18G', salt: '$2a$10$uZuKQpHyqP0ZfDBPEG/Ine', token: '87ed61ca64a47dfaf5e80ab5739fdfba', remember_token: 'cde89be6e2d324dd4d3ae531dff83c8a', email: 'test23@test.ru', avatar: 'avatar23.jpg', created_at: new Date()});

coll.save({username: 'test24', realname: 'Test24 Testov', password: '$2a$10$Gvl33IpPHkT2PNIPFAoXOOv2bmtNVETJO85DIyB7XcgOeRZeigHD6', salt: '$2a$10$Gvl33IpPHkT2PNIPFAoXOO', token: 'f87de1b62d1b660f144adeef8ff8c5c7', remember_token: '3b4977c6e5c6e5302adeb3095d8f314a', email: 'test24@test.ru', avatar: 'avatar24.jpg', created_at: new Date()});

coll.save({username: 'test25', realname: 'Test25 Testov', password: '$2a$10$smjU/.Lit8G19OYRbVwDkenoMDWzaln7YLeGyA7a4ef5BloC2flPC', salt: '$2a$10$smjU/.Lit8G19OYRbVwDke', token: '3b19074fa64892956e9d915698a39168', remember_token: 'ea72351b9d43b7ede0e7fb03401c9da4', email: 'test25@test.ru', avatar: 'avatar25.jpg', created_at: new Date()});

coll.save({username: 'test26', realname: 'Test26 Testov', password: '$2a$10$s5aW3HSeLprp4n5B1RssIeQR70ER3JzDQJUMKYgL1X03NaVWeenu6', salt: '$2a$10$s5aW3HSeLprp4n5B1RssIe', token: 'f8f861497f8cf45e3835481bdf340aa0', remember_token: '105289c3f9c2da8b5706cd35a8669180', email: 'test26@test.ru', avatar: 'avatar26.jpg', created_at: new Date()});

coll.save({username: 'test27', realname: 'Test27 Testov', password: '$2a$10$UBsHM5Chwalz76CjF/1ggO6/ym.BoWknCBuN2qkC674tBshruEbf2', salt: '$2a$10$UBsHM5Chwalz76CjF/1ggO', token: 'bcb5db43afe8af3732d70949de52aa78', remember_token: '01b8e7159b5ef3e7fd1fc4deaacf06f0', email: 'test27@test.ru', avatar: 'avatar27.jpg', created_at: new Date()});

coll.save({username: 'test28', realname: 'Test28 Testov', password: '$2a$10$bjMOowMYoqPrUQkNDoJ/uuLVbOnUdH85XtmmitQrlv/FfdHqLCGei', salt: '$2a$10$bjMOowMYoqPrUQkNDoJ/uu', token: '852b5cfdc1a9521acffb5084582d4ac3', remember_token: 'aaad3c2bff5d06742c5b3e3e0e2196a3', email: 'test28@test.ru', avatar: 'avatar28.jpg', created_at: new Date()});

coll.save({username: 'test29', realname: 'Test29 Testov', password: '$2a$10$JWBlYgsuClELSSwWEMogiuom3H0H0kU2aSIkFwPZDfMl78GpZ8T3q', salt: '$2a$10$JWBlYgsuClELSSwWEMogiu', token: 'a8187ef2ca4005c63712d32661b72887', remember_token: 'f9098ad65f369016243965d1cb7fbee1', email: 'test29@test.ru', avatar: 'avatar29.jpg', created_at: new Date()});

coll.save({username: 'test30', realname: 'Test30 Testov', password: '$2a$10$d7WJPJtgt25LpBLDJtxw5OvUS7B03lDvGb/8AYFUx8.1yjf9j7qHW', salt: '$2a$10$d7WJPJtgt25LpBLDJtxw5O', token: 'df5bca768aa77e605d15b2a48d7d3f34', remember_token: 'cf3324045e9a1669f6088f0cbb14536f', email: 'test30@test.ru', avatar: 'avatar30.jpg', created_at: new Date()});

coll.save({username: 'test31', realname: 'Test31 Testov', password: '$2a$10$Mrf8YzH.wBgw8iQxeyUg..mD4rlKfrydt1N2xrUgughbzMfg8DsaK', salt: '$2a$10$Mrf8YzH.wBgw8iQxeyUg..', token: 'b74bdf743da4596976f54462c32cd843', remember_token: '68ee11abf4beed50f505555f2992dd1d', email: 'test31@test.ru', avatar: 'avatar31.jpg', created_at: new Date()});

coll.save({username: 'test32', realname: 'Test32 Testov', password: '$2a$10$q5qjK8eddP83m69xZTmtSe4a84xwp1Cp6hy0Q3QXIVo9Us7Q5upMm', salt: '$2a$10$q5qjK8eddP83m69xZTmtSe', token: 'b5d218c323384a71100d9d96feac20ca', remember_token: '02eeaa642c4af10db6d67ccef1a13286', email: 'test32@test.ru', avatar: 'avatar32.jpg', created_at: new Date()});

coll.save({username: 'test33', realname: 'Test33 Testov', password: '$2a$10$y5aFIgP3/GfzWNVV2S.1/uq0dnWOq2SlJE7GVhL8ErhW5J/K.oyhW', salt: '$2a$10$y5aFIgP3/GfzWNVV2S.1/u', token: 'de2728a9b3051568158904731dd85efc', remember_token: '111f3252ec41eeb72889a687a901928d', email: 'test33@test.ru', avatar: 'avatar33.jpg', created_at: new Date()});

coll.save({username: 'test34', realname: 'Test34 Testov', password: '$2a$10$LkOwIDzGTyFYDhgQ50gkKuTPThjw3KbBpvJj/p6U6o0etx5bNa4Eu', salt: '$2a$10$LkOwIDzGTyFYDhgQ50gkKu', token: '1d95fd6418de3aab63675d6c7caf13d5', remember_token: 'e7f2e523c93a7681755716981e6a5aea', email: 'test34@test.ru', avatar: 'avatar34.jpg', created_at: new Date()});

coll.save({username: 'test35', realname: 'Test35 Testov', password: '$2a$10$/bE7CYPSDKO9mqYvH/OS6e5xunFRqBuB5Yu6ak55ddPbOnA5bmqQO', salt: '$2a$10$/bE7CYPSDKO9mqYvH/OS6e', token: '62a4a4e152d09be2ff53686b730df9a2', remember_token: '4b8067b8059bec22b5faad049059b4ed', email: 'test35@test.ru', avatar: 'avatar35.jpg', created_at: new Date()});

coll.save({username: 'test36', realname: 'Test36 Testov', password: '$2a$10$0fMps.2Pycosx9XhUWO5t.54pgmZZ0tC1j0cPpU85VEI8VVhiXx9K', salt: '$2a$10$0fMps.2Pycosx9XhUWO5t.', token: 'b0cbbfe2880f9d4f29b4a9b54fa90720', remember_token: '0674fcee8a0a28d66ff92138b36a4df1', email: 'test36@test.ru', avatar: 'avatar36.jpg', created_at: new Date()});

coll.save({username: 'test37', realname: 'Test37 Testov', password: '$2a$10$DV3J44ozkvP4apNw0P2YguYiP2AjRjw7BYLvw6miWDFHCiLHxepTi', salt: '$2a$10$DV3J44ozkvP4apNw0P2Ygu', token: 'aac14b6eab7c2dce0ae8fa11275671fc', remember_token: '30ef09efd34fa7364f869974cb5b28d3', email: 'test37@test.ru', avatar: 'avatar37.jpg', created_at: new Date()});

coll.save({username: 'test38', realname: 'Test38 Testov', password: '$2a$10$9p8KhgMN4rRIVRpjDo1OZeORnZx.9yVvLaLki.BbiPeACe98jSwp2', salt: '$2a$10$9p8KhgMN4rRIVRpjDo1OZe', token: 'de664496c894fe4ce04b836e96143448', remember_token: '50ed6975ed499709c6292c1c10f472ab', email: 'test38@test.ru', avatar: 'avatar38.jpg', created_at: new Date()});

coll.save({username: 'test39', realname: 'Test39 Testov', password: '$2a$10$pWbx02HmLkb3HeXt01k3uugw9HFnJ8gmGNqt173fU.fM0lJ4O8sz6', salt: '$2a$10$pWbx02HmLkb3HeXt01k3uu', token: 'a34836f6e139fead5311048bd59ca2da', remember_token: '7df38c7565ddb14a08680da0154dad57', email: 'test39@test.ru', avatar: 'avatar39.jpg', created_at: new Date()});

coll.save({username: 'test40', realname: 'Test40 Testov', password: '$2a$10$KhsrRyI1jtVEqy5G/dwNa.NuL6dQTdJUr2DwrfcGl3mpWgPvqKQoi', salt: '$2a$10$KhsrRyI1jtVEqy5G/dwNa.', token: '0903e7346e65fac7c1bb64f18403f6df', remember_token: 'b6b5b8a74c8f76f51f105575139ccf80', email: 'test40@test.ru', avatar: 'avatar40.jpg', created_at: new Date()});

coll.save({username: 'test41', realname: 'Test41 Testov', password: '$2a$10$plcjLLRW927J7pnXDIS4Iukwbz4dZS2s/gJM/Jcw3VDdORy8B8LIC', salt: '$2a$10$plcjLLRW927J7pnXDIS4Iu', token: '8bc7d03f49746c05e6862fc8516210e8', remember_token: '93c94e3bea15381b2ffbc2eb717979af', email: 'test41@test.ru', avatar: 'avatar41.jpg', created_at: new Date()});

coll.save({username: 'test42', realname: 'Test42 Testov', password: '$2a$10$uzRlGDXsDdFOgFkRYpPP7.f6s22E0./ooYlfZxL/G.H0fSumo8pYy', salt: '$2a$10$uzRlGDXsDdFOgFkRYpPP7.', token: 'f1e2db29c827eb1584883ca6d0d3a2ce', remember_token: '6ab9163af985a68eefa08003c0d6d454', email: 'test42@test.ru', avatar: 'avatar42.jpg', created_at: new Date()});

coll.save({username: 'test43', realname: 'Test43 Testov', password: '$2a$10$Zv1QxkLm6qcToWTes4k4VewtzHuoHDBENDeNnxMpXo6pJ.Uc5BYye', salt: '$2a$10$Zv1QxkLm6qcToWTes4k4Ve', token: '8808e46d2e22347369263aafa5ab3836', remember_token: '6cc0063ff10a23c66fc4fc84e79a7dae', email: 'test43@test.ru', avatar: 'avatar43.jpg', created_at: new Date()});

coll.save({username: 'test44', realname: 'Test44 Testov', password: '$2a$10$9AZMg00vXJYKQEoBAheUB.Q/Ffdud7QQWd6ZjIdwiW.auwzHrImQa', salt: '$2a$10$9AZMg00vXJYKQEoBAheUB.', token: '743a59a8cf818a5fb67a1326a44475d6', remember_token: 'a362b7d947b4f7b944aa92fa9d46af2d', email: 'test44@test.ru', avatar: 'avatar44.jpg', created_at: new Date()});

coll.save({username: 'test45', realname: 'Test45 Testov', password: '$2a$10$knb5vxBYF1IvXSbmscICRuEDoMfNdabDaUvt7Z0igyRiKMd02Q4GC', salt: '$2a$10$knb5vxBYF1IvXSbmscICRu', token: '22c59cf5510a8c0e3ace509dcb59f5de', remember_token: 'f2aa34d310c435e8cc42db7dfc99a027', email: 'test45@test.ru', avatar: 'avatar45.jpg', created_at: new Date()});

coll.save({username: 'test46', realname: 'Test46 Testov', password: '$2a$10$Qa2NwbwV/SlZ.F0c5XW7UeC83xUgjKmg0gF8WHScA/yJB9b.kBm9a', salt: '$2a$10$Qa2NwbwV/SlZ.F0c5XW7Ue', token: '557fcca1a892ff33fcdefbae36579f76', remember_token: '22e82fc84b5fb19267f201e01fab6df5', email: 'test46@test.ru', avatar: 'avatar46.jpg', created_at: new Date()});

coll.save({username: 'test47', realname: 'Test47 Testov', password: '$2a$10$NgzKVvDp9euxzt2LbVxznuyiAI0W6ZRgwZ5jcMghtDyBM3Ia78XXa', salt: '$2a$10$NgzKVvDp9euxzt2LbVxznu', token: 'ac2b8a2e77ab838107c0c38e71bcfd2f', remember_token: '4a9181d28c8e274f5b2822e70c31f15c', email: 'test47@test.ru', avatar: 'avatar47.jpg', created_at: new Date()});

coll.save({username: 'test48', realname: 'Test48 Testov', password: '$2a$10$8o.jXLDGfYjuaU/og6NCoOdCF.R8GlPcedrZeMbUn7AivthlVdyAy', salt: '$2a$10$8o.jXLDGfYjuaU/og6NCoO', token: 'a5fe1d852623dff88b51f1de83b36ce4', remember_token: '72885e214aa2adfd35689ab5c9cfb2f3', email: 'test48@test.ru', avatar: 'avatar48.jpg', created_at: new Date()});

coll.save({username: 'test49', realname: 'Test49 Testov', password: '$2a$10$hCL2XfXwrr7mMs37YhcpR.WLDQkgroN0rixFb7GNfDkb8qMJ2AtsW', salt: '$2a$10$hCL2XfXwrr7mMs37YhcpR.', token: 'b9dad75d61c08793d06325f77b834b8f', remember_token: 'fcc91afed974b6a8bfb3fc16c1194595', email: 'test49@test.ru', avatar: 'avatar49.jpg', created_at: new Date()});

coll.save({username: 'test50', realname: 'Test50 Testov', password: '$2a$10$wehZXkp2mPC3sOosMdUuYeYuEvDj5B4hmi36aWPNkh/mUUi.DVUpS', salt: '$2a$10$wehZXkp2mPC3sOosMdUuYe', token: '121b71f12d1c2e2f9f8a7b1bd97964b1', remember_token: '88d77ead283e8b0ededf1baf0862695b', email: 'test50@test.ru', avatar: 'avatar50.jpg', created_at: new Date()});

coll.save({username: 'test51', realname: 'Test51 Testov', password: '$2a$10$42nT9M1RvqdnrZKLdOrda.SKhE/b2ZNOG4coteiJWBlLfqZI1ZbgO', salt: '$2a$10$42nT9M1RvqdnrZKLdOrda.', token: '4476f7b16d3623813c32dba0c6419778', remember_token: '7a451dc3cc9ad01a4a4b0c6af244b58c', email: 'test51@test.ru', avatar: 'avatar51.jpg', created_at: new Date()});

coll.save({username: 'test52', realname: 'Test52 Testov', password: '$2a$10$iEgxT9C3OWvonouQlnCHzeTJThhySLfbAkuEkNwnmNV3IOI8qSsjy', salt: '$2a$10$iEgxT9C3OWvonouQlnCHze', token: 'cce71e4ccfae553bb22ffb018215ef31', remember_token: 'c86f97a0ceed6f422b30f245e2dd18fc', email: 'test52@test.ru', avatar: 'avatar52.jpg', created_at: new Date()});

coll.save({username: 'test53', realname: 'Test53 Testov', password: '$2a$10$lf3WFNMUBHT9NHeM0OlsFOrJkcg/KkJG0Nz2b/rB6YjSJXEIaewG2', salt: '$2a$10$lf3WFNMUBHT9NHeM0OlsFO', token: 'a1c40a1a755fef80540d1156255ea280', remember_token: '10b5ca0b6d53f669a209841a46af9014', email: 'test53@test.ru', avatar: 'avatar53.jpg', created_at: new Date()});

coll.save({username: 'test54', realname: 'Test54 Testov', password: '$2a$10$oBG.pPL7xwFK9Q0ti0thb.7MUhhelfEV/6nHFWlva51WT8wBqhi2e', salt: '$2a$10$oBG.pPL7xwFK9Q0ti0thb.', token: '7aefcafae35c305b120a6e1aeb1a485b', remember_token: '0086e9e64b826959ee771af5d993df9a', email: 'test54@test.ru', avatar: 'avatar54.jpg', created_at: new Date()});

coll.save({username: 'test55', realname: 'Test55 Testov', password: '$2a$10$zU/Fx4JVU9fU5Z0D5ktiyuy05QT1zIaO64i0u0yhW0AKBmMcjWZcm', salt: '$2a$10$zU/Fx4JVU9fU5Z0D5ktiyu', token: '650c929b5c6ee46946f367d49c20181c', remember_token: 'a97d01b0a6c658fcfccf553ca203ccc3', email: 'test55@test.ru', avatar: 'avatar55.jpg', created_at: new Date()});

coll.save({username: 'test56', realname: 'Test56 Testov', password: '$2a$10$LY34WGgV5Rv5D26tlly6pOx6AMLF6A9U3Yn2l1qR5fFArXda2N0Vu', salt: '$2a$10$LY34WGgV5Rv5D26tlly6pO', token: '728c5a29ef1dfa0f14c7919862dd827c', remember_token: '80cdd2c27af1ae5a080b2d45d296bfc9', email: 'test56@test.ru', avatar: 'avatar56.jpg', created_at: new Date()});

coll.save({username: 'test57', realname: 'Test57 Testov', password: '$2a$10$OFFzeF2JPvbVkXgl8CweS.U1uQStVeIjXIWkqhEDJEdQX5/pKUmZe', salt: '$2a$10$OFFzeF2JPvbVkXgl8CweS.', token: 'eabfa95503b6a395f2d70c242d72875a', remember_token: 'a2064a2ea2e6a570de7bf10b1d77595f', email: 'test57@test.ru', avatar: 'avatar57.jpg', created_at: new Date()});

coll.save({username: 'test58', realname: 'Test58 Testov', password: '$2a$10$6Gi3a/.fbeC9JKwD/B/zzOIwuWnxTxT0l7rQ2dA5m8t5ZN1Jw6KTe', salt: '$2a$10$6Gi3a/.fbeC9JKwD/B/zzO', token: 'ea0c994a65e1d8aec2439233611b8261', remember_token: '9f13368e8af111770d3c1fdff6eafe2e', email: 'test58@test.ru', avatar: 'avatar58.jpg', created_at: new Date()});

coll.save({username: 'test59', realname: 'Test59 Testov', password: '$2a$10$.y2wkBKSwV2al0MMTiE4ue9nBYgPMGqyrZ0BjqKL5bYfsmModgJNy', salt: '$2a$10$.y2wkBKSwV2al0MMTiE4ue', token: '2fbc85f55c2831defce6ce1bae7fa73f', remember_token: 'ddb88d4edb6815b5a54c62503ae040c5', email: 'test59@test.ru', avatar: 'avatar59.jpg', created_at: new Date()});

coll.save({username: 'test60', realname: 'Test60 Testov', password: '$2a$10$n5qA92IGL/1nPntpgsz9SevQAAlNaMU2GZrum1ydD3b8aNP6kkQuq', salt: '$2a$10$n5qA92IGL/1nPntpgsz9Se', token: '894a286367b5b945861fc3f505f1224a', remember_token: 'c1c6cf80b558cb29b4d38ba16c64fef3', email: 'test60@test.ru', avatar: 'avatar60.jpg', created_at: new Date()});

coll.save({username: 'test61', realname: 'Test61 Testov', password: '$2a$10$93irWmSJZlBVlvz0KwKZYO260PMXtZe1WOg3SfhSC5zV9NoVFdoPS', salt: '$2a$10$93irWmSJZlBVlvz0KwKZYO', token: '00588e5420af86d71ccd774cec44d811', remember_token: '1b7a3032044f2bb6804ef5b56c26ebee', email: 'test61@test.ru', avatar: 'avatar61.jpg', created_at: new Date()});

coll.save({username: 'test62', realname: 'Test62 Testov', password: '$2a$10$YmdzOZW4y1ZSEhdGd3.04uEmrXkbn/laCGRfCKf6fYw5674JZUPOW', salt: '$2a$10$YmdzOZW4y1ZSEhdGd3.04u', token: 'd988b9b559ba517d72e96ffa883fbc4d', remember_token: 'f4ff088003b938dd640d745b72dcd1ef', email: 'test62@test.ru', avatar: 'avatar62.jpg', created_at: new Date()});

coll.save({username: 'test63', realname: 'Test63 Testov', password: '$2a$10$2mRYMlpGpO3sWFXmKnG4VucGmCUSp9EMfFLkAMGthdM99hldxqtRu', salt: '$2a$10$2mRYMlpGpO3sWFXmKnG4Vu', token: '2612f066619f9cb57b205110020f765e', remember_token: '27c3babb02778215f8b68fc28d87673d', email: 'test63@test.ru', avatar: 'avatar63.jpg', created_at: new Date()});

coll.save({username: 'test64', realname: 'Test64 Testov', password: '$2a$10$KgKev6N9LdeBrQB7oLEAc.ai2/2aqRE4BesX9YFDNn6s5Z9juGgB.', salt: '$2a$10$KgKev6N9LdeBrQB7oLEAc.', token: '517647191a5fe79731bbbe0c9af25a48', remember_token: '90872b6ed1d98eee6156a0b0715af436', email: 'test64@test.ru', avatar: 'avatar64.jpg', created_at: new Date()});

coll.save({username: 'test65', realname: 'Test65 Testov', password: '$2a$10$m/35mfgwiCK7I8CNcI1zAOsFSPCTG92MXsw/7ruoWQWXiSmPy.5Ry', salt: '$2a$10$m/35mfgwiCK7I8CNcI1zAO', token: '44d8a6cbe8f30c8ce881bf5c08620d6b', remember_token: '3ac97bb16cae6b745cc0551a5be44c4b', email: 'test65@test.ru', avatar: 'avatar65.jpg', created_at: new Date()});

coll.save({username: 'test66', realname: 'Test66 Testov', password: '$2a$10$2duLUOPXYk2anDdlT.7VuuzuMJbentYt0Ya.KgNk.DeL8Pj2sqozm', salt: '$2a$10$2duLUOPXYk2anDdlT.7Vuu', token: '4552299c60576cb239b49e5815a4eaf2', remember_token: '1d160375efd2eb2c8fb24fa9d8a9b104', email: 'test66@test.ru', avatar: 'avatar66.jpg', created_at: new Date()});

coll.save({username: 'test67', realname: 'Test67 Testov', password: '$2a$10$ytRhmu3xh16PPHlTthsEnu2HIflPXOMsFKEiUVBTON4FLJewD3F7S', salt: '$2a$10$ytRhmu3xh16PPHlTthsEnu', token: '61fd6843a4d92364a87fec1645d1df8c', remember_token: '4a59f4a4403d9616cf51c393740e4927', email: 'test67@test.ru', avatar: 'avatar67.jpg', created_at: new Date()});

coll.save({username: 'test68', realname: 'Test68 Testov', password: '$2a$10$729Z9iXOX4ZAjnH7AO2Sh.4SJ3qeSjDr4CZhspc/bYGYUBq1Sm7cW', salt: '$2a$10$729Z9iXOX4ZAjnH7AO2Sh.', token: 'b9529d8f90aafb6ae9b85519289d2d6c', remember_token: '189e26c5a0163b5dcbc253ef9658099f', email: 'test68@test.ru', avatar: 'avatar68.jpg', created_at: new Date()});

coll.save({username: 'test69', realname: 'Test69 Testov', password: '$2a$10$zuI7zx0h1YyygPqUqJ0.meQ9.A7G08W7cAmJfbSEwLOMfaVFW8AXO', salt: '$2a$10$zuI7zx0h1YyygPqUqJ0.me', token: '93fc2ed5a50b1677be33f25008b618bc', remember_token: '6c9a078e5d60aa5f9910299fe8321add', email: 'test69@test.ru', avatar: 'avatar69.jpg', created_at: new Date()});

coll.save({username: 'test70', realname: 'Test70 Testov', password: '$2a$10$NCuMZU0wQ3yj7MATwcEB5OeVEhzw02/fQe3ECmKzKOvr4C9goEspC', salt: '$2a$10$NCuMZU0wQ3yj7MATwcEB5O', token: '9f148207dac613b79878dbc54d7d0ce8', remember_token: '738d4bd4133de98469f78f8e8fb1ca49', email: 'test70@test.ru', avatar: 'avatar70.jpg', created_at: new Date()});

coll.save({username: 'test71', realname: 'Test71 Testov', password: '$2a$10$Cxn3Dvz4yEwkHb4HuP7Kjel3XuG0utvUosMnhjCLkkrAkErgHmfBq', salt: '$2a$10$Cxn3Dvz4yEwkHb4HuP7Kje', token: 'd13b64ea8d48973bbaa870fb279c6374', remember_token: '8a016ebc7a0f5ee767c34442a7dac94e', email: 'test71@test.ru', avatar: 'avatar71.jpg', created_at: new Date()});

coll.save({username: 'test72', realname: 'Test72 Testov', password: '$2a$10$7asGbPohZHIx4uuBStt5e.zRv1CnmakxOmRgk1ywSC9GrBfFZH8eG', salt: '$2a$10$7asGbPohZHIx4uuBStt5e.', token: '79817a25be89859d3cf90bd31d3e1da4', remember_token: '96411a70f042abb4ec6a2018c42519df', email: 'test72@test.ru', avatar: 'avatar72.jpg', created_at: new Date()});

coll.save({username: 'test73', realname: 'Test73 Testov', password: '$2a$10$moQ/qfsRZWbzUCS9H.Uh4ONpsuhXbkMXXabeM6q64ATF8YHMQC9GG', salt: '$2a$10$moQ/qfsRZWbzUCS9H.Uh4O', token: '13e6138b41b3931b79ae649a3ede0823', remember_token: '4571c04d596aeb935111e905f60332d0', email: 'test73@test.ru', avatar: 'avatar73.jpg', created_at: new Date()});

coll.save({username: 'test74', realname: 'Test74 Testov', password: '$2a$10$Fa90dQIQ0cZofoDbftLa9.ZlotNCFlKzLF9Mh7lrkGnpa5kxQCr2m', salt: '$2a$10$Fa90dQIQ0cZofoDbftLa9.', token: '0beceea355abd1c68ddb8562373ebf17', remember_token: '8d06c3fd267f1c8eaeffbcdf42fd1fff', email: 'test74@test.ru', avatar: 'avatar74.jpg', created_at: new Date()});

coll.save({username: 'test75', realname: 'Test75 Testov', password: '$2a$10$1bnnlDXwm0ZItkGpyUAJmu9WMidcNkAlYQpW/AurZa5Fqqic901ZO', salt: '$2a$10$1bnnlDXwm0ZItkGpyUAJmu', token: 'cc1aca7b766bf6e8ec8295a20d6a1abb', remember_token: 'd83f60a0ab69fc82aaeeea5c43d8af0f', email: 'test75@test.ru', avatar: 'avatar75.jpg', created_at: new Date()});

coll.save({username: 'test76', realname: 'Test76 Testov', password: '$2a$10$zategS0I/4EcScIyWSRJ1OsSGotyq9aQs7Ep3v1pJceGFqmJVFXq2', salt: '$2a$10$zategS0I/4EcScIyWSRJ1O', token: '8aac32580534d332ecba9a585e4fcfc7', remember_token: 'cc3bb4f461f5068868772e8763f2c243', email: 'test76@test.ru', avatar: 'avatar76.jpg', created_at: new Date()});

coll.save({username: 'test77', realname: 'Test77 Testov', password: '$2a$10$pf6.MrzPTNgEeYPm769XWOsdD7Ko2QHMzAyjTv9FNohBDbLgj7Kpi', salt: '$2a$10$pf6.MrzPTNgEeYPm769XWO', token: '546b29bfa92a492b654e29375a8f1bda', remember_token: '6191ce92b999bdd04fea7855fb12f91a', email: 'test77@test.ru', avatar: 'avatar77.jpg', created_at: new Date()});

coll.save({username: 'test78', realname: 'Test78 Testov', password: '$2a$10$SbOcvypXFSpWOJUIbthDYuT7sqirafg.xIdQXpvg5n8XLlmCIJeR6', salt: '$2a$10$SbOcvypXFSpWOJUIbthDYu', token: 'a52bb4adfcf32f7644a487daca7c1981', remember_token: '504e549c34eb01381004991a022749b4', email: 'test78@test.ru', avatar: 'avatar78.jpg', created_at: new Date()});

coll.save({username: 'test79', realname: 'Test79 Testov', password: '$2a$10$.Xa8XFl7rrAUbgA2nKrXteyIP6cRoHpBoh/Ua2oMZxCC3pI7YjFbu', salt: '$2a$10$.Xa8XFl7rrAUbgA2nKrXte', token: '3ab0e2185b1b0d906fe57033ab967cb2', remember_token: 'b352c26e1e6d0548dc7a71cb40dde267', email: 'test79@test.ru', avatar: 'avatar79.jpg', created_at: new Date()});

coll.save({username: 'test80', realname: 'Test80 Testov', password: '$2a$10$Hm893DXl42opyAT7SDd72eVRTgUIgb0ox9pN7AnjgN8OvALtYa9xS', salt: '$2a$10$Hm893DXl42opyAT7SDd72e', token: '3e2773d64fd7004bfad1301c215dacbf', remember_token: '588220cfe9f9c3553c8de15608a35692', email: 'test80@test.ru', avatar: 'avatar80.jpg', created_at: new Date()});

coll.save({username: 'test81', realname: 'Test81 Testov', password: '$2a$10$6Aoeg2wHluTlrhjmclcOEe6Dx2lmYlwPNIP2OGFBHkNYC1sshKzh.', salt: '$2a$10$6Aoeg2wHluTlrhjmclcOEe', token: 'f2afda01fa0f03e70f70bca662f77355', remember_token: 'fc61ac0d5ad9f37c9e8578fa53ec06c1', email: 'test81@test.ru', avatar: 'avatar81.jpg', created_at: new Date()});

coll.save({username: 'test82', realname: 'Test82 Testov', password: '$2a$10$l4OgHC3C9cIT1DfC/1OCD.5hoHqB//nXmxAeUgCY1lyCkdY4f4oOe', salt: '$2a$10$l4OgHC3C9cIT1DfC/1OCD.', token: 'e442c3046bd8191561445d1032535640', remember_token: '5dc396b175c853e322eb191a1af607e3', email: 'test82@test.ru', avatar: 'avatar82.jpg', created_at: new Date()});

coll.save({username: 'test83', realname: 'Test83 Testov', password: '$2a$10$a8wBziK0PzldQ9cARgSrG.Ht.8OV3hZUBLq8sBudEPFXIoKDkgqjG', salt: '$2a$10$a8wBziK0PzldQ9cARgSrG.', token: '2fa42423cd183f8d808b3ade1eed93c5', remember_token: 'cd8993d9b1b7824c695c6531d1a6f556', email: 'test83@test.ru', avatar: 'avatar83.jpg', created_at: new Date()});

coll.save({username: 'test84', realname: 'Test84 Testov', password: '$2a$10$vfuRbMeqc5oKVKHbXW14Xup8/AnlVLV55N3vBY7TvNVOUP8Z9T7cm', salt: '$2a$10$vfuRbMeqc5oKVKHbXW14Xu', token: 'ab1ef44a528d7da8a7585a1c35d9e3d8', remember_token: '20f62accf19ae4abb9f4cdaeb330e939', email: 'test84@test.ru', avatar: 'avatar84.jpg', created_at: new Date()});

coll.save({username: 'test85', realname: 'Test85 Testov', password: '$2a$10$ZX3QgfcNIQdbjtmR/.y2UeRy.2uKd6XalYWY9.GoMj2vylLo2Iz3K', salt: '$2a$10$ZX3QgfcNIQdbjtmR/.y2Ue', token: '2b0d7096788d73282749337e8da73f0b', remember_token: '16c8f3f382fe92fe964737096bcb489c', email: 'test85@test.ru', avatar: 'avatar85.jpg', created_at: new Date()});

coll.save({username: 'test86', realname: 'Test86 Testov', password: '$2a$10$eFTUjn29.b8dx.KzUrJBLuGi6xY2k3rB5FTThdBc1YRwT.FzLZdPy', salt: '$2a$10$eFTUjn29.b8dx.KzUrJBLu', token: '53fae7b8b9cf5ab010f3ec6ba8b4b92f', remember_token: '956dd59bfb57d906632b10263af3524b', email: 'test86@test.ru', avatar: 'avatar86.jpg', created_at: new Date()});

coll.save({username: 'test87', realname: 'Test87 Testov', password: '$2a$10$pObVd3cLmg1f1/eAIzHvl./4kl5JneGV/L7yezMPB.ZsQcX3dxLHy', salt: '$2a$10$pObVd3cLmg1f1/eAIzHvl.', token: '004a1e80a5e7df93e5b55554678a9481', remember_token: '4c3e4708ddf0039d891bef14dd45987e', email: 'test87@test.ru', avatar: 'avatar87.jpg', created_at: new Date()});

coll.save({username: 'test88', realname: 'Test88 Testov', password: '$2a$10$M3gso36WvRnGFcE9wWSI5uDpgZqMGCRqRN5in2b8LWMYzNSvCR1zG', salt: '$2a$10$M3gso36WvRnGFcE9wWSI5u', token: '2e06dec15355c4669276de989b4fc7e1', remember_token: '7a56f178c104ccf983a4b5c50d99c1b1', email: 'test88@test.ru', avatar: 'avatar88.jpg', created_at: new Date()});

coll.save({username: 'test89', realname: 'Test89 Testov', password: '$2a$10$UEwIcAxPCeRmfwPvcJRQvuXlcHHlqj3.FNNZz3yEV.83qjsnSR4Y6', salt: '$2a$10$UEwIcAxPCeRmfwPvcJRQvu', token: '950e79d974370f3d203585ecdd55a0ac', remember_token: '5331ee5b8aad70fb612b714db89d725f', email: 'test89@test.ru', avatar: 'avatar89.jpg', created_at: new Date()});

coll.save({username: 'test90', realname: 'Test90 Testov', password: '$2a$10$tiyFGPtSUvd1FbNtIeH0XeKyXXhOILtba.5mfdeam3FhYawvz7BCW', salt: '$2a$10$tiyFGPtSUvd1FbNtIeH0Xe', token: '857087edd9de6e80a0e48bb8fecdbd2d', remember_token: 'bad71c8bef688f6119b01b921b33c916', email: 'test90@test.ru', avatar: 'avatar90.jpg', created_at: new Date()});

coll.save({username: 'test91', realname: 'Test91 Testov', password: '$2a$10$OwoaqU94yn2D0z7c7iD60OZ75RPIQ3vqu0Di.z39dThBGz/2A4wTa', salt: '$2a$10$OwoaqU94yn2D0z7c7iD60O', token: '0b8fd917e6a254cd2be953230d1b088b', remember_token: '3d751ba3b483960d709f779db92418b0', email: 'test91@test.ru', avatar: 'avatar91.jpg', created_at: new Date()});

coll.save({username: 'test92', realname: 'Test92 Testov', password: '$2a$10$7zGiKOnHfFCP1UzNpAoApO06FkERLcrPlPpb3Sp4tWdR8OA.sdLqO', salt: '$2a$10$7zGiKOnHfFCP1UzNpAoApO', token: '09b557fb2794ad4da38894a5cca7a58d', remember_token: '8691bd97d91704a75d38501c7575d4fe', email: 'test92@test.ru', avatar: 'avatar92.jpg', created_at: new Date()});

coll.save({username: 'test93', realname: 'Test93 Testov', password: '$2a$10$.61eKyt3Jf07ll5CUCbcnu5KvwWmOlmUgC2jkUUK4EhHottmZlLl.', salt: '$2a$10$.61eKyt3Jf07ll5CUCbcnu', token: '1d1553e43b15b4c6ca1e5b82aa14a366', remember_token: '89be9dad744add3bca1f1c2b62ccc288', email: 'test93@test.ru', avatar: 'avatar93.jpg', created_at: new Date()});

coll.save({username: 'test94', realname: 'Test94 Testov', password: '$2a$10$9Em8flk/nUVqSTbl3QSl3eyYwNbMPRRqicilNVDSZ7z1xRf5osnRS', salt: '$2a$10$9Em8flk/nUVqSTbl3QSl3e', token: 'c4d0eed9b9088a0f615bdd83807fa131', remember_token: 'cab46c7605c207bc4970ae9790c2d20a', email: 'test94@test.ru', avatar: 'avatar94.jpg', created_at: new Date()});

coll.save({username: 'test95', realname: 'Test95 Testov', password: '$2a$10$3W72Ly67DU9kLoMajE2VSOqaaOZW1/vKjzTxOKhEOft5yMpwZUeRG', salt: '$2a$10$3W72Ly67DU9kLoMajE2VSO', token: 'd0076c752e8f73f9b9233cac01cc623e', remember_token: 'af05cbca0c8f5a2501b924692071fa89', email: 'test95@test.ru', avatar: 'avatar95.jpg', created_at: new Date()});

coll.save({username: 'test96', realname: 'Test96 Testov', password: '$2a$10$C8IrfXMtcvytYqS.zDo81.KocZHpljGGGcSxx9IkElrUGeQRqaHL.', salt: '$2a$10$C8IrfXMtcvytYqS.zDo81.', token: '45a86fed4da131071958374f9dce1cbf', remember_token: 'a80d244b7fca756568d53e5c123c408b', email: 'test96@test.ru', avatar: 'avatar96.jpg', created_at: new Date()});

coll.save({username: 'test97', realname: 'Test97 Testov', password: '$2a$10$Pr/fJ2oBKOUXOWDtitCuj.ndw0/zvk5MWlLFmlQID8uljcVW/3ZLK', salt: '$2a$10$Pr/fJ2oBKOUXOWDtitCuj.', token: '98fd54cff75d0fbad39a6aff21016501', remember_token: '0d56b1bf0a28018167d170bcfda0895f', email: 'test97@test.ru', avatar: 'avatar97.jpg', created_at: new Date()});

coll.save({username: 'test98', realname: 'Test98 Testov', password: '$2a$10$zaAlpjVMYtlIv.RAw48lReCWAXAo27WD4AL3M4Ona3NBJ6vfY08ae', salt: '$2a$10$zaAlpjVMYtlIv.RAw48lRe', token: '4c909c8c5e319e0a378f696acc74d5ab', remember_token: 'e19b8ed1519ea9c3656840aafeb2da09', email: 'test98@test.ru', avatar: 'avatar98.jpg', created_at: new Date()});

coll.save({username: 'test99', realname: 'Test99 Testov', password: '$2a$10$XhWVIllsqvhWGt8/bMgxROmDL4FXhv3Iq7JcYSSsSHTJUxtZTY7Nu', salt: '$2a$10$XhWVIllsqvhWGt8/bMgxRO', token: 'e4533d2e089e2478ba128b3505488e85', remember_token: '98d39681e99b5999b6ad9e1e4ba67347', email: 'test99@test.ru', avatar: 'avatar99.jpg', created_at: new Date()});
