// -- Models --

App.User = DS.Model.extend({
  username: DS.attr('string'),
  realname: DS.attr('string'),
  email: DS.attr('string'),
  avatar: DS.attr('string'),
  password: DS.attr('string'),
  created_at: DS.attr('date'),
  isAuthenticated: function(){
    return (this.get('id') == App.Auth.get('userId'));
  }.property('id')   
});

App.Job = DS.Model.extend({
  author: DS.attr('string'),
  title: DS.attr('string'),
  description: DS.attr('string'),
  completed: DS.attr('boolean'),
  created_at: DS.attr('date')
});