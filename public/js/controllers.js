App.UserController = Ember.ObjectController.extend({
  saveUser: function(){
    this.get('store').commit();
  }
});

App.UsersNewController = Ember.ObjectController.extend({
  createUser: function(){
    var user = App.User.createRecord(this.content);
    console.log(user);
    this.get('store').commit();
  }
});