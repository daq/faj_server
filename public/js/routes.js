// App.UsersRoute = Ember.Route.extend({
//   redirect: function() {
//     this.transitionTo('users.page');
//   }
// });

// App.UsersIndexRoute = Ember.Route.extend({
//   model: function(){
//     if (App.Auth.get('signedIn')){
//       App.User.find();
//     }
//   }
// });

// App.UsersShowRoute = Ember.Route.extend({
//   renderTemplate: 'show-user',
//   serialize: function(model){
//     {user_id: model.get('param')}
//   },
//   model: function(param){
//     if(App.Auth.get('signedIn')){
//       return App.User.find(param.user_id);
//     }
//   }
// });

App.UsersRoute = Ember.Route.extend({
  model: function(){
    return App.User.find();
  }
});

App.UsersNewRoute = Ember.Route.extend({
  setupController: function(controller, model){
    controller.set('content', {});
  }
});